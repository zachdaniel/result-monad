class Error

  include ResultMethods

  def initialize(err)
    @err = err
  end

  def handle_result(success_handler, failure_handler)
    failure_handler.call(@err)
  end

end