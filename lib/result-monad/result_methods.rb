module ResultMethods
  #TODO:
  # map! Not necessarily a good idea
  # map_unsafe! See above
  def map
    handle_result(
      Proc.new {|val| Capture { yield(val) } },
      Proc.new {|err| self }
    )
  end

  def map_err
    handle_result(
      Proc.new {|val| self },
      Proc.new {|val| Capture { yield(val) } }
    )
  end

  def join
    handle_result(
      Proc.new { |x| x.is_a?(Ok) || x.is_a?(Error) ? x.join : self },
      Proc.new { |x| self }
    )
  end

  def flat_map(&block)
    (map &block).join
  end

  def map_unsafe
    handle_result(
      Proc.new {|val| yield(val) },
      Proc.new {|err| self }
    )
  end

  def and_then
    handle_result(
      Proc.new {|val| yield(val); self},
      Proc.new {|err| self}
    )
  end

  def or_else
    handle_result(
      Proc.new {|val| self},
      Proc.new {|err| yield(err); self}
    )
  end

  def flat_map_unsafe(&block)
    (map_unsafe &block).join
  end

  def unwrap
    handle_result(
      Proc.new {|val| val},
      Proc.new do |err|
        if err.is_a? Exception
          raise err
        else
          raise ResultError.new, "#{err}"
        end
      end
    )
  end

  def ok?
    is_a? Ok
  end

  def error?
    is_a? Error
  end

  def to_s
    "#{ self.class.name }: #{@result}"
  end
end