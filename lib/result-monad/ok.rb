class Ok

  include ResultMethods

  def initialize(result)
    @result = result
  end

  def handle_result(success_handler, failure_handler)
    success_handler.call(@result)
  end

end