require 'result-monad/version'
require 'result-monad/result_error'
require 'result-monad/result_methods'
require 'result-monad/error'
require 'result-monad/ok'

def Error(error)
  Error.new(error)
end

def Ok(value)
  Ok.new(value)
end

def Test(o, e)
  if yield
    Ok(o)
  else
    Error(e)
  end
end

def Capture
  begin
    Ok(yield)
  rescue StandardError => e
    Error(e)
  end
end

