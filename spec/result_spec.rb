require 'spec_helper'

RSpec.describe Result do
  let (:good) { "A good result!" }
  let (:bad)  { Exception.new("Error") }
  let (:err)  { Error(bad) }
  let (:okay) { Ok(good) }

  it 'has a version number' do
    expect(Result::VERSION).not_to be nil
  end

  it 'provides the Error constructor' do
    expect(Error(Exception.new("Error"))).to be_an_instance_of(Error)
  end

  it 'provides the Ok constructor' do
    expect(Ok("A good result!")).to be_an_instance_of(Ok)
  end

  describe 'Capture' do
    it 'returns a Result::Ok if no exception' do
      expect(Capture {"A good result!"}).to be_ok
    end

    it 'returns a Result::Error if an exception occurs' do
      expect(Capture {raise "an error"}).to be_error
    end
  end

  describe 'Test' do
    xit {}
  end

  describe 'unwrap' do
    context 'error' do
      it 'raises an exception' do
        expect {err.unwrap}.to raise_exception(Exception)
      end

      xit 'raises an ResultError exception'
    end

    context 'ok' do
      it 'returns the wrapped value' do
        expect(okay.unwrap).to eq(good)
      end
    end
  end

  describe 'map' do
    context 'error' do
      it 'leaves the object alone' do
        expect(err.map {|x| x.not_a_method_on_error}).to eq(err)
      end

      it 'does not call the provided block' do
        proc = Proc.new {|x| x.still_not_a_method}
        expect {|proc| err.map &proc}.not_to yield_control
      end
    end

    context 'ok' do
      it 'calls the provided block' do
        proc = Proc.new {|x| x.split}
        expect {|proc| okay.map &proc}.to yield_with_args(good)
      end

      it 'returns the value of the block called with x' do
        proc = Proc.new {|x| x + "Something Else"}
        expect(okay.map(&proc).unwrap).to eq(good + "Something Else")
      end
    end
  end

  describe 'map_err' do
    xit {}
  end

  describe 'join' do
    xit {}
  end

  describe 'flat_map' do
    xit {}
  end

  describe 'map_unsafe' do
    xit {}
  end

  describe 'flat_map_unsafe' do
    xit {}
  end

  describe 'or_else' do
    context 'ok' do
      it 'does not execute the provided block' do
        proc = Proc.new {puts "Whatsup"}
        expect(proc).not_to receive(:call)
        okay.or_else(&proc)
      end
    end

    context 'error' do
      it 'executes the provided block' do
        proc = Proc.new {puts "Whatsup"}
        expect {|proc| err.or_else(&proc)}.to yield_with_args(bad)
      end
    end
  end

  describe 'and_then' do
    context 'ok' do
      it 'executes the provided block' do
        proc = Proc.new {puts "Whatsup"}
        expect {|proc| okay.and_then(&proc)}.to yield_with_args(good)
      end
    end

    context 'error' do
      it 'does not execute the provided block' do
        proc = Proc.new {puts "Whatsup"}
        expect {|proc| err.and_then(&proc)}.not_to yield_control
      end
    end
  end
end
