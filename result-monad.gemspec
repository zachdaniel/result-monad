# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'result-monad/version'

Gem::Specification.new do |spec|
  spec.name          = "result-monad"
  spec.version       = Result::VERSION
  spec.authors       = ["Zachary Daniel"]
  spec.email         = ["zachary.s.daniel@gmail.com"]

  spec.summary       = "The Result Monad implemented in ruby."
  spec.description   = "The Result Monad implemented in ruby. Designed to maximize usefulness, as sticking to the category theoretical Monad is not really feasible without an evolved static type system."
  spec.homepage      = "http://gitlab.com/zachdaniel/result-monad"
  spec.license       = "MIT"

  spec.files         = `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  spec.bindir        = "exe"
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]
  spec.required_ruby_version = '~> 2.0'

  spec.add_development_dependency "bundler", "~> 1.10"
  spec.add_development_dependency "rake", "~> 10.0"
  spec.add_development_dependency "rspec"
  spec.add_development_dependency "byebug"
end
